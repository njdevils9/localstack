data "aws_iam_policy_document" "localstack_iam_allow_ec2_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy" "localstack_iam_grant_admin_access" {
  arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_role" "localstack_iam_role" {
  name = "jon-localstack-iam-role"
  path = "/"
  description = "This is a test IAM role created with Terraform and is deployed to LocalStack."
  assume_role_policy = data.aws_iam_policy_document.localstack_iam_allow_ec2_assume_policy.json
}

resource "aws_iam_role_policy_attachment" "localstack_iam_grant_admin_access_policy_attachment" {
  role = aws_iam_role.localstack_iam_role.name
  policy_arn = data.aws_iam_policy.localstack_iam_grant_admin_access.arn
}

