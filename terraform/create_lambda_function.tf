resource "aws_lambda_function" "my_lambda" {
  filename      = "../source/lambda.zip"
  function_name = "My-Lambda-Function"
  role          = aws_iam_role.localstack_iam_role.arn
  handler       = "lambda.main"
  source_code_hash = filebase64sha256("../source/lambda.zip")
  runtime = "python3.6"
  
  tags = {
    Name = "LocalStack-Lambda"
  }
}

