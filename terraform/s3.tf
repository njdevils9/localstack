resource "aws_s3_bucket" "localstack_create_s3_bucket" {
  bucket = "jon-localstack-bucket-${var.REGION}-0002"

  acl = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags = {
    ChargeCode = "LocalStack-FREE"
    ManagedBy  = "Terraform"
    DeployedTo = "LocalStack Environment"
  }
}
