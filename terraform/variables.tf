variable "REGION" {
   description = "Region deployed to inside of LocalStack environment."
   default = "us-east-1"
}
