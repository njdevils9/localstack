output "LOCAL_STACK_IAM_ROLE_ARN" {
  description = "The ARN of the IAM role created inside of Jon's LocalStack environment."
  value = aws_iam_role.localstack_iam_role.arn
}

output "LOCAL_STACK_IAM_USER_NAME" {
  description = "LocalStack IAM user."
  value = "${aws_iam_user.localstack_iam_user.name}"
}

output "LOCAL_STACK_IAM_USER_ACCESS_KEY_ID" {
  description = "LocalStack IAM user's access key ID."
  value = join("", aws_iam_access_key.localstack_iam_user_access_key.*.id)
}

output "LOCAL_STACK_IAM_USER_SECRET_ACCESS_KEY" {
  description = "LocalStack IAM user's  secret access key."
  value = join("", aws_iam_access_key.localstack_iam_user_access_key.*.secret)
}
