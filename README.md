## LocalStack

Thanks for checking out my proof of concept using LocalStack. To view the documentation for this repo, please follow the following steps. Alternatively, if you do not wish to install mkdocs, you can open up each of the files under the `docs` directory and view it using a Markdown preview plugin in your text editor.


Installing Mkdocs
------------------
`mkdocs` is used for providing markdown documentation via your web browser. Clone this repo and `cd` into the localstack directory. If you don't have mkdocs already installed please do so...
```bash
[jon@laptop ~]$ git clone git@gitlab.com:njdevils9/localstack.git
[jon@laptop ~]$ cd localstack
[jon@laptop ~]$ pip install --user --upgrade pip
[jon@laptop ~]$ pip install --user mkdocs
[jon@laptop ~]$ mkdocs --version
```

Viewing Documentation
---------------------
Once you have cloned the repo and have mkdocs installed, please run the following commands. If you are not familiar with mkdocs, it will launch the docs on localhost on specific port.
```bash
[jon@laptop ~]$ mkdocs serve
    INFO    -  Building documentation...
    INFO    -  Cleaning site directory
    WARNING -  Documentation file 'index.md' contains a link to 'installation.md' which is not found in the documentation files.
    INFO    -  Documentation built in 0.18 seconds
    [I 200608 06:57:46 server:296] Serving on http://127.0.0.1:8000
    INFO    -  Serving on http://127.0.0.1:8000
    [I 200608 06:57:46 handlers:62] Start watching changes
    INFO    -  Start watching changes
```

Now you can open your web browser and go to: http://127.0.0.1:8000 to view the remaining documentation.
