# Welcome


Please checkout the each tab to learn a little more about LocalStack.

A lot of this information has been copied straight from the publicly available GitHub repo but I went ahead and outlined a few things that stood out to me that I believe could benefit us immediately so that you don't have to read through the entire thing if you don't have time. Definitely take a look at the rest of the documentation if you have time.

https://github.com/localstack/serverless-localstack

Please follow documentation in this order...
---------------------------------------------

1. [Home](./index.md)
2. [About](./about.md)
3. [Installation](./installation.md)
4. [AWS_CLI_Demo](./demo/aws_cli_demo.md)
5. [Terraform_Demo](./demo/terraform_demo.md)
6. [Pulumi_Demo](./demo/pulumi_demo.md)
7. [Cost](./pricing.md)
