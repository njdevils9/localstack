# Terraform Demo Against LocalStack Infrastructure

*Note: I am using Terraform version 12.24 for this demo. Visit the link below to retrieve the binary*

- https://releases.hashicorp.com/terraform/


## Navigate to wherever you have this code stored in
```bash
[jmurillo_sa@laptop ~]$ cd ~/Repos/systems/jon_sandbox/localstack/terraform/
[jmurillo_sa@laptop ~]$ ls -lah
drwxrwxr-x. 4 jmurillo_sa jmurillo_sa  153 Jun  5 11:25 .
drwxrwxr-x. 4 jmurillo_sa jmurillo_sa   53 Jun  5 10:57 ..
-rw-rw-r--. 1 jmurillo_sa jmurillo_sa   76 Jun  5 11:24 backend.tf
-rw-rw-r--. 1 jmurillo_sa jmurillo_sa  858 Jun  5 11:07 iam_role_creation.tf
-rw-r--r--. 1 jmurillo_sa jmurillo_sa  407 Jun  5 11:07 iam_user_creation.tf
-rw-rw-r--. 1 jmurillo_sa jmurillo_sa  176 Jun  5 11:12 outputs.tf
-rw-rw-r--. 1 jmurillo_sa jmurillo_sa 1.2K Jun  5 11:02 provider.tf
-rw-rw-r--. 1 jmurillo_sa jmurillo_sa  414 Jun  5 11:25 s3.tf
drwxr-xr-x. 3 jmurillo_sa jmurillo_sa   65 Jun  5 11:25 .terraform
drwxr-xr-x. 3 jmurillo_sa jmurillo_sa   19 Jun  5 11:25 terraform.tfstate.d
-rw-rw-r--. 1 jmurillo_sa jmurillo_sa  119 Jun  5 11:11 variables.tf
```

## Checking Terraform Version
```bash
[jmurillo_sa@laptop ~]$ terraform --version
Terraform v0.12.24
+ provider.aws v2.64.0
+ provider.null v2.1.2

Your version of Terraform is out of date! The latest version
is 0.12.26. You can update by downloading from https://www.terraform.io/downloads.html
```

## Create a new workspace "infra"
```bash
[jmurillo_sa@laptop ~]$ terraform init
[jmurillo_sa@laptop ~]$ terraform workspace list
  default
[jmurillo_sa@laptop ~]$ terraform workspace new infra
Created and switched to workspace "infra"!

You are now on a new, empty workspace. Workspaces isolate their state,
so if you run "terraform plan" Terraform will not see any existing state
for this configuration.

[jmurillo_sa@laptop ~]$ terraform workspace list
  default
* infra
```

## Running a Terraform plan
```bash
[jmurillo_sa@laptop ~]$ terraform plan
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.

data.aws_iam_policy.localstack_iam_grant_admin_access: Refreshing state...
data.aws_iam_policy_document.localstack_iam_allow_ec2_assume_policy: Refreshing state...

------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_iam_role.localstack_iam_role will be created
  + resource "aws_iam_role" "localstack_iam_role" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + Service = "ec2.amazonaws.com"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + description           = "This is a test IAM role created with Terraform and is deployed to LocalStack."
      + force_detach_policies = false
      + id                    = (known after apply)
      + max_session_duration  = 3600
      + name                  = "jmurillo-localstack-iam-role"
      + path                  = "/"
      + unique_id             = (known after apply)
    }

  # aws_iam_role_policy_attachment.localstack_iam_grant_admin_access_policy_attachment will be created
  + resource "aws_iam_role_policy_attachment" "localstack_iam_grant_admin_access_policy_attachment" {
      + id         = (known after apply)
      + policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
      + role       = "jmurillo-localstack-iam-role"
    }

  # aws_s3_bucket.localstack_create_s3_bucket will be created
  + resource "aws_s3_bucket" "localstack_create_s3_bucket" {
      + acceleration_status         = (known after apply)
      + acl                         = "private"
      + arn                         = (known after apply)
      + bucket                      = "jmurillo-localstack-userdata-bucket-us-east-1"
      + bucket_domain_name          = (known after apply)
      + bucket_regional_domain_name = (known after apply)
      + force_destroy               = false
      + hosted_zone_id              = (known after apply)
      + id                          = (known after apply)
      + region                      = (known after apply)
      + request_payer               = (known after apply)
      + tags                        = {
          + "ChargeCode" = "LocalStack-FREE"
          + "DeployedTo" = "LocalStack Environment"
          + "ManagedBy"  = "Terraform"
        }
      + website_domain              = (known after apply)
      + website_endpoint            = (known after apply)

      + server_side_encryption_configuration {
          + rule {
              + apply_server_side_encryption_by_default {
                  + sse_algorithm = "AES256"
                }
            }
        }

      + versioning {
          + enabled    = (known after apply)
          + mfa_delete = (known after apply)
        }
    }

    # aws_iam_access_key.localstack_iam_user_access_key will be created
    + resource "aws_iam_access_key" "localstack_iam_user_access_key" {
        + encrypted_secret     = (known after apply)
        + id                   = (known after apply)
        + key_fingerprint      = (known after apply)
        + secret               = (sensitive value)
        + ses_smtp_password    = (sensitive value)
        + ses_smtp_password_v4 = (sensitive value)
        + status               = (known after apply)
        + user                 = "LocalStack_IAM_User"
      }

    # aws_iam_user.localstack_iam_user will be created
    + resource "aws_iam_user" "localstack_iam_user" {
        + arn           = (known after apply)
        + force_destroy = false
        + id            = (known after apply)
        + name          = "LocalStack_IAM_User"
        + path          = "/"
        + unique_id     = (known after apply)
      }

      # aws_iam_user_policy_attachment.localstack_iam_user_policy_attachment will be created
      + resource "aws_iam_user_policy_attachment" "localstack_iam_user_policy_attachment" {
          + id         = (known after apply)
          + policy_arn = "LocalStack_IAM_User:LocalStack_IAM_User"
          + user       = "LocalStack_IAM_User"
        }


Plan: 6 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.
```

## Running a Terraform apply
```bash
[jmurillo_sa@laptop ~]$ terraform apply
.
..
...
Apply complete! Resources: 3 added, 0 changed, 0 destroyed.

The state of your infrastructure has been saved to the path
below. This state is required to modify and destroy your
infrastructure, so keep it safe. To inspect the complete state
use the `terraform show` command.

State path: terraform.tfstate

Outputs:

LOCAL_STACK_IAM_ROLE_ARN = arn:aws:iam::000000000000:role/jmurillo-localstack-iam-role
LOCAL_STACK_IAM_USER_ACCESS_KEY_ID = none-ya-business
LOCAL_STACK_IAM_USER_NAME = LocalStack_IAM_User
LOCAL_STACK_IAM_USER_SECRET_ACCESS_KEY = none-ya-business
```

## Lets Review Our Terraform  State (stored locally)
```bash
[jmurillo_sa@laptop ~]$ terraform state list
data.aws_iam_policy.localstack_iam_grant_admin_access
data.aws_iam_policy_document.localstack_iam_allow_ec2_assume_policy
aws_iam_access_key.localstack_iam_user_access_key
aws_iam_role.localstack_iam_role
aws_iam_role_policy_attachment.localstack_iam_grant_admin_access_policy_attachment
aws_iam_user.localstack_iam_user
aws_iam_user_policy_attachment.localstack_iam_user_policy_attachment
aws_s3_bucket.localstack_create_s3_bucket
```

## Let's confirm the location of our new S3 bucket and IAM role to make sure it wasn't accidentally deployed to one of our accounts.
```bash
[jmurillo_sa@laptop ~]$ aws --endpoint-url=http://localhost:4566 s3 ls
2020-06-05 10:54:38 jmurillo-localstack-bucket-us-east-1-0001
2020-06-05 10:54:51 jmurillo-localstack-bucket-us-west-1-0001
2020-06-05 11:17:34 jmurillo-localstack-userdata-bucket-us-east-1
2020-06-05 11:26:11 jmurillo-localstack-bucket-us-east-1-0002   <---- Terraform just created this :D
```
