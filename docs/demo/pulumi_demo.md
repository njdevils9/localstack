# Pulumi Demo Against LocalStack Infrastructure | WIP

## Installation
```bash
[jmurillo_sa@laptop ~]$ curl -fsSL https://get.pulumi.com | sh

## restart your shell
[jmurillo_sa@laptop ~]$ exec bash
[jmurillo_sa@laptop ~]$ pulumi version
v2.3.0
```

## Create a new project from a template

```bash
[jmurillo_sa@laptop ~]$ pulumi new python --name s3bucket-localstack
Manage your Pulumi stacks by logging in.
Run `pulumi login --help` for alternative login options.
Enter your access token from https://app.pulumi.com/account/tokens
    or hit <ENTER> to log in using your browser                   :
We have ve launched your web browser to complete the login process.

Waiting for login to complete...


  Welcome to Pulumi!

  Pulumi helps you create, deploy, and manage infrastructure on any cloud using
  your favorite language. You can get started today with Pulumi at:

      https://www.pulumi.com/docs/get-started/

  Tip of the day: Resources you create with Pulumi are given unique names (a randomly
  generated suffix) by default. To learn more about auto-naming or customizing resource
  names see https://www.pulumi.com/docs/intro/concepts/programming-model/#autonaming.


This command will walk you through creating a new Pulumi project.

Enter a value or leave blank to accept the (default), and press <ENTER>.
Press ^C at any time to quit.

project description: (A minimal Python Pulumi program) S3 bucket creation against LocalStack AWS infrastructure
Created project 's3bucket-localstack'

Please enter your desired stack name.
To create a stack in an organization, use the format <org-name>/<stack-name> (e.g. `acmecorp/dev`).
stack name: (dev) infra
Created stack 'infra'

Your new project is ready to go!

To perform an initial deployment, run the following commands:

   1. python3 -m venv venv
   2. source venv/bin/activate
   3. pip3 install -r requirements.txt

Then, run 'pulumi up'

## Python and Pip are already default on my OS so no need to run pip3
[jmurillo_sa@laptop ~]$ pip install --user -r requirements.txt
```

```bash
[jmurillo_sa@laptop ~]$ pulumi up
Enter your passphrase to unlock config/secrets
    (set PULUMI_CONFIG_PASSPHRASE to remember):
Previewing update (pulumi-localstack-test):
     Type                     Name                                        Plan       Info
 +   pulumi:pulumi:Stack      s3bucket-localstack-pulumi-localstack-test  create     
 +   ├─ pulumi:providers:aws  provider                                    create     
     └─ aws:s3:Bucket         jmurillo-pulumi-localstack-bucket                      1 error

Diagnostics:
  aws:s3:Bucket (jmurillo-pulumi-localstack-bucket):
    error: error using credentials to get account ID: error calling sts:GetCallerIdentity: InvalidClientTokenId: The security token included in the request is invalid.
    	status code: 403, request id: 9c5ced65-ef23-4c4b-b443-4a38274eb50d

```

## Attempting to request a one-time token in case the one created from Terraform isn't working.
```bash
[jmurillo_sa@laptop ~]$ aws --endpoint-url=http://localhost:4566 sts get-session-token --duration-seconds 10000
```

## At this point I either receive a 403 error. Or complains about the endpoint. If I switch my AWS profile to Demarc, it works immediately.. The expected out looks something like this (this created a new bucket in Demarc)..
```bash
[jmurillo_sa@laptop ~]$ pulumi up
Enter your passphrase to unlock config/secrets
    (set PULUMI_CONFIG_PASSPHRASE to remember):
Previewing update (pulumi-localstack-test):
     Type                     Name                                        Plan       
 +   pulumi:pulumi:Stack      s3bucket-localstack-pulumi-localstack-test  create     
 +   ├─ pulumi:providers:aws  provider                                    create     
 +   ├─ aws:s3:Bucket         jmurillo-pulumi-localstack-bucket           create     
 +   ├─ aws:s3:BucketPolicy   bucket-policy                               create     
 +   └─ aws:s3:BucketObject   index.html                                  create     

Resources:
    + 5 to create

Do you want to perform this update? yes
Updating (pulumi-localstack-test):
     Type                     Name                                        Status                  Info
 +   pulumi:pulumi:Stack      s3bucket-localstack-pulumi-localstack-test  **creating failed**     1 error
 +   ├─ pulumi:providers:aws  provider                                    created                 
 +   ├─ aws:s3:Bucket         jmurillo-pulumi-localstack-bucket           created                 
 +   ├─ aws:s3:BucketPolicy   bucket-policy                               **creating failed**     1 error
 +   └─ aws:s3:BucketObject   index.html                                  created                 

Diagnostics:
  pulumi:pulumi:Stack (s3bucket-localstack-pulumi-localstack-test):
    error: update failed

  aws:s3:BucketPolicy (bucket-policy):
    error: Error putting S3 policy: MalformedPolicy: Policy has invalid resource
    	status code: 400, request id: DB099F578161DE9D, host id: 55Qr0r0RGnYxQWz2mwMjdaffuXSGmCsFMj+Ielo4bjKKOMcZHycng3rZLnGDYxzcpTUqmgHa4HU=

Outputs:
    bucket_name: "jmurillo-pulumi-localstack-bucket-229cd3a"
    website_url: "jmurillo-pulumi-localstack-bucket-229cd3a.s3-website-us-east-1.amazonaws.com"

Resources:
    + 4 created

Duration: 1m14s
```
