# AWS CLI Demo Against LocalStack Infrastructure

First, you need to see which port the service is available at. For me, it appears like all of the services are available on port 4566 based on the terminal output.

So, I can now do something like this:
```bash
## Accessing IAM
[jmurillo_sa@laptop ~]$ aws --endpoint-url=http://localhost:4566 iam list-instance-profiles
{
    "InstanceProfiles": []
}

## Accessing STS
[jmurillo_sa@laptop ~]$ aws --endpoint-url=http://localhost:4566 sts get-caller-identity
{
    "UserId": "AKIAIOSFODNN7EXAMPLE",
    "Account": "123456789012",
    "Arn": "arn:aws:sts::123456789012:user/moto"
}

## creating S3 buckets in different regions
[jmurillo_sa@laptop ~]$ aws --endpoint-url=http://localhost:4566 s3api create-bucket --bucket jmurillo-localstack-bucket-us-east-1-0001 --region us-east-1
[jmurillo_sa@laptop ~]$ aws --endpoint-url=http://localhost:4566 s3api create-bucket --bucket jmurillo-localstack-bucket-us-west-1-0001 --region us-west-1

## validating S3 buckets exist
[jmurillo_sa@laptop ~]$ aws --endpoint-url=http://localhost:4566 s3 ls
2020-06-05 10:54:38 jmurillo-localstack-bucket-us-east-1-0001
2020-06-05 10:54:51 jmurillo-localstack-bucket-us-west-1-0001
```
