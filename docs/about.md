# Brief Description
`LocalStack` is an app that provides an easy-to-use test/mocking framework for developing Cloud applications. Currently, the focus is primarily on supporting the AWS.

LocalStack spins up the following core Cloud API's on your local machine. A few in the free tier are:

    - API Gateway
    - Kinesis
    - DynamoDB
    - DynamoDB Streams
    - S3
    - Firehose
    - Lambda
    - SNS
    - SQS
    - SES
    - Route53
    - CloudFormation
    - CloudWatch
    - SSM
    - SecretsManager
    - StepFunctions
    - CloudWatch Logs
    - EventBridge (CloudWatch Events)
    - STS
    - IAM
    - EC2
    - KMS

Whenever you pay for the Pro version, you start getting access to services such as:

    - API Gateway v2 (websocket support)
    - Athena
    - CloudFront
    - ECS/EKS
    - ElastiCache
    - Lambda Layers
    - RDS
    - Interactive UI's to manage resources
    - Test report dashboards
    - etc.

Terraform
---------
You can test/development with Terraform using LocalStack, in which case LocalStack depends on using the Terraform AWS provider with conditionals set.

- **Terraform LocalStack Provider:**
      - https://www.terraform.io/docs/providers/aws/guides/custom-service-endpoints.html#localstack

Serverless
----------
You can use the `serverless-localstack` plugin to run Serverless apps on LocalStack.

- https://github.com/localstack/serverless-localstack

Dashboard
---------
There is a simple web dashboard available that allows you to view the deployed AWS components and the relationship between them. Just run this in your terminal:
```bash
[jmurillo_sa@laptop ~]$ localstack web
```

There are other UI clients available that support LocalStack and AWS:

- **Commandeer:** *(definitely a little pricey when moving out of the Free version but checkout the feature breakdown)*
    - https://getcommandeer.com/
    - https://app.getcommandeer.com/pricing
<br>
</br>
- **DynamoDB Admin Web UI:** *(free and installs via npm)*
    - https://www.npmjs.com/package/dynamodb-admin
