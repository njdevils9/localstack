# Installation

Requirements
------------
- python
- pip
- Docker
- LinuxOS or MacOS. It doesn't appear to be supported in Windows

```bash
[jmurillo_sa@laptop ~]$ pip install --user localstack
```

*At the time of running this article, I ran into some issues trying to run this on Docker on my local machine. To move forward, I just started LocalStack locally*

Running in Docker
------------------
This will download a localstack Docker container and start making each of the AWS services available to you on a specific port on localhost.

NOTE: The first time downloading might take a little while for all services to become available. A more light-weight image is available (`localstack/localstack-light`) which has some large dependency files excluded. This image might be the default in the future.
```bash
## standard install
[jmurillo_sa@laptop ~]$ localstack start

## using localstack-light
[jmurillo_sa@laptop ~]$ export USE_LIGHT_IMAGE=1
[jmurillo_sa@laptop ~]$ localstack start
```

Starting Locally (without Docker)
---------------------------------
```bash
[jmurillo_sa@laptop ~]$ localstack start --host
```
